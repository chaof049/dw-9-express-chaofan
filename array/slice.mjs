let ar=['a','b','c','d','e']

// slice has start index and end index inside the parentheses
// 1 is start index and 4 is the end index
// the end index must always be +1 then the desired end output
// let ar1=ar.slice(1,4)  
// output = ['b','c','d']

// if end index is missing in slice method
// it will return all the elements from start index to the end
let ar1=ar.slice(2) 
console.log(ar)
console.log(ar1)