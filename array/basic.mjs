// let ar=["chao",24,true]
//array can store multiple value of different data type

//get whole array
// console.log(ar)

//get specific array
// console.log(ar[0])
// console.log(ar[1])
// console.log(ar[2])

//change specific element of array
// ar[1]=79
// ar[2]=false
// console.log(ar[1])
// console.log(ar[2])

//delete specific element of array
// delete ar[1]
// console.log(ar)

let ar=["dave","jon","kev"]
console.log(ar)
console.log(ar[2])
delete ar[1]
console.log(ar)
ar[2]="doe"
console.log(ar)