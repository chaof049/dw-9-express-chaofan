let ar1 = [1,2,[1,3],[[2,4],[3,6]]]

let ar2 = ar1.flat(1) // [ 1, 2, 1, 3, [ 2, 4 ], [ 3, 6 ] ]
let ar3 = ar1.flat(2) // [ 1, 2, 1, 3, 2, 4, 3, 6 ]; 2 means the number of wrapper to be opened

console.log(ar2)
console.log(ar3)