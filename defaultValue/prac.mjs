let fun = (a, b, c, [friend1, friend2], { name, age }) => {
  console.log(a);
  console.log(b);
  console.log(c);
  console.log(friend1);
  console.log(friend2);
  console.log(name);
  console.log(age);
};

fun(1, 2, true, ["jon", "doe"], { name: "jack", age: 44 });
