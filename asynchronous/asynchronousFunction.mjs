// setTimeout and setInterval function always executes at last
// they pass the function in the node and 
// it will further pass into the memory queue after the set timer and 
// will only returns after all the code has been executed in js and 
// will execute the function
// anything that pushes a task to the background is called asynchronous function
// event loop: it constantly monitors call stack if it is empty and 
// takes the function from the memory queue and
// add it to the call stack

console.log("a");
setTimeout(() => {
  console.log("timeout");
}, 0);
console.log("b");


