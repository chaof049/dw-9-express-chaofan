let listStr = JSON.stringify([1, 2, 3]); // '[1,2,3]'
let objStr = JSON.stringify({ name: "chao" }); // '{name:chao}'
let n = JSON.stringify(null); // 'null'

console.log(listStr);
console.log(objStr);
console.log(n);
