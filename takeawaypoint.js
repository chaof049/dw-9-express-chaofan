// Js is a single threaded synchronous blocking programming language
// Data
// Number - 1,2,...
// String - always wrapped by quote(double or single)
// Boolean - true or false
// js is case sensitive
// operators
// +, -, *, /
// 1+2 = 3 (sum)number
// "1"+"2" = 12(concatenation)string
// 1+"2"="12" -  if there is a war between string and number. string always wins
// operations are done between two operand(two values)
// variable is a container that stores data
// js is a loosely typed language, a variable can store any value including number, string and boolean
// eg: let a = 3; a="sup"; a=true
// comparator operator , eg: 1===1 ;output(t), 3!==1;output(f)
// && = true; if all are true, || = true; if one is true
// variable name must be descriptive and if two words or more must follow camel case convention
// to convert into boolean ,  number case: except 0 all(1,2,...) are true, string case: all('q','0',' ') the strings are true except empty('')
// while writing the code in if...else statement, always write the priority in first statement.
// the code written after 'return' does not execute
// function without 'return' => call without storing in variable
// function with 'return' => call with storing variable
// all method in loop return something but push,pop,unshift and shift change the original array while reverse and sort change both
// number sort doesn't work properly in javascript with sort function
// setTimeout and setInterval function always executes at last
// they pass the function in the node and
// it will further pass into the memory queue after the set timer and
// will only returns after all the code has been executed in js and
// will execute the function
// anything that pushes a task to the background is called asynchronous function
// event loop: it constantly monitors call stack if it is empty and
// takes the function from the memory queue and
// add it to the call stack
// JSON - javascript object notation
