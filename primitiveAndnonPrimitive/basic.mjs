// primitive : string, int, boolean, undefined
// non primitive : array, object
// primitive creates two memory when doing 'let', '===' checks the value
// non primitive shares memory when doing 'let', '===' checks if they share the same memory

// primitive
// let a = 1;
// let b = a;
// a = 3;
// console.log(a);
// console.log(b);

// non-primitive
// let ar1 = [1, 2];
// let ar2 = ar1;
// ar1.push(3);
// console.log(ar1);
// console.log(ar2);

// ===
// primitive
// let a = 1;
// let b = a;
// let c = 1;
// console.log(a === b);
// console.log(a === c);

// non primitive
// let ar1 = [1, 2];
// let ar2 = ar1;
// let ar3 = [1, 2];
// console.log(ar1 === ar2);
// console.log(ar1 === ar3);

// let ar1=[1,2]
// let ar2=ar1
// console.log(ar1===ar2) => the output is true
// console.log([1,2]===[1,2]) => the output is false because the data is not stored in any variable
