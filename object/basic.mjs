//array is made by the combination of value
//object is made by the combination of key value pairs
//name=>key , 'chao'=>value, name:'chao'=>property
// let obj={
//     name:'chao',
//     age:67,
//     isMarried:false
// }
//get whole object
// console.log(obj)

//get specific elements
// console.log(obj.name)
// console.log(obj.age)
// console.log(obj.isMarried)

//change specific element
// obj.age=46
// console.log(obj)

//delete specific element
// delete obj.age
// console.log(obj)

// let obj={
//     name:'chao',
//     location:'wuhan',
//     contactNumber:15537591639,
// }
// console.log(obj)
// obj.location='gagalphedi'
// console.log(obj)
// delete obj.contactNumber
// console.log(obj)

let obj={
    name:'chao',
    age:33,
}
//add object
obj.address='china'
console.log(obj)