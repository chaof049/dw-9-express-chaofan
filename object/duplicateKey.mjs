//duplicate key doesn't exist in object
//if there is duplication, latest code overwrites old code

let obj={
    name:"chao",
    age:21,
    age:56
}
console.log(obj) //{ name: 'chao', age: 56 }

obj.name="fan"
console.log(obj)//{ name: 'fan', age: 56 }