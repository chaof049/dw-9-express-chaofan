// we cannot convert all array to object
// we can only convert if we have array like [ [ 'name', 'chao' ], [ 'age', 44 ], [ 'location', 'wuhan' ] ]
// here we have array of array and inner array has length of 2

let ar = [
  ["name", "chao"],
  ["age", 44],
  ["location", "wuhan"],
];

let info = Object.fromEntries(ar); // { name: 'chao', age: 44, location: 'wuhan' }
console.log(info);
