let obj={
    name:'chao',
    age:44,
    location:'wuhan'
}

let keyArr=Object.keys(obj)
let valueArr=Object.values(obj)
let propArr=Object.entries(obj)

console.log(keyArr) // [ 'name', 'age', 'location' ]
console.log(valueArr) // [ 'chao', 44, 'wuhan' ]
console.log(propArr) // [ [ 'name', 'chao' ], [ 'age', 44 ], [ 'location', 'wuhan' ] ]