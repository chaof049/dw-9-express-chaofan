let { age, name, location } = { name: "chao", location: "wuhan", age: 33 };

// in object order doesn't matter
console.log(name);
console.log(location);
console.log(age);
