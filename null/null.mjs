// null is used to empty the variable
// never assign variable by undefined value

let a;
console.log(a);

a = null;
console.log(null);
