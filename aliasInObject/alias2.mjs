// let { name: name1, age: age1 = 33 } = { name: "chao"};
// // always place the value after the changed name

// console.log(name1);
// console.log(age1);

// let { name='china': countryName, area } = { name: "nepal", area: "147181" }
let { name: countryName= 'china', area } = { name: "nepal", area: "147181" }
console.log(countryName)
