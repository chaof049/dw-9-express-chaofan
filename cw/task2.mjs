// find the product of all the element using reduce method

let ar = [1, 2, 3, 4, 5];

let value = ar.reduce((pre, cur) => {
  return pre * cur;
}, 1);
console.log(value);
