// stringify
// let input = [1, 2, 1, ["a", "b"], ["a", "b"], ["a", "b"]];
// let openar = input.map((value, i) => {
//   return JSON.stringify(value);
// });

// let openset = [...new Set(openar)];

// let setparse = openset.map((value, i) => {
//   return JSON.parse(value);
// });
// console.log(setparse);

// using function

let fun = (input) => {
  let openAr = input.map((value, i) => {
    return JSON.stringify(value);
  });
  let openSet = [...new Set(openAr)];
  let setParse = openSet.map((value, i) => {
    return JSON.parse(value);
  });
  return setParse;
};

console.log(fun([1, 1, ["a"], ["a"]]));
