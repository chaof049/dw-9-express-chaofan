let products = [
  { name: "earphone", price: 1000 },
  { name: "battery", price: 2000 },
  { name: "charger", price: 500 },
];

let value = products.reduce((pre,cur)=>{
    return pre+cur.price
},0)
console.log(value)

let name1 = products.map((value, i) => {
  return value.name;
});
console.log(name1)

let price1 = products.map((value, i) => {
  return value.price;
});
console.log(price1);

let priceGreaterThan700=products.filter((value,i)=>{
    if (value.price > 700)
        return value
})

let price3 = priceGreaterThan700.map((value, i) => {
  return value.price;
});
console.log(price3);

let name2 = priceGreaterThan700.map((value, i) => {
  return value.name;
});
console.log(name2);